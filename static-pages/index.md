<!-- <img src="/img/mielho.png" alt="mielho logo" style="width:100%; max-width:324px; height:auto;"> -->

Welcome to **mielho**, a gently curated catalog of output by honeybees. Information on all the <a href="/honies" class="view">listings</a> is free for anyone to view or use however they wish, however, the details of some listings may *only* be visible on logging in. Please read more <a href="/about" class="view">about</a> how that works, or meet our <a href="/bees" class="view">honeybees</a>.

**Note:** <a href="/lessons?lightweight=true" class="view">Load the listings without images</a> for a less sticky experience.
