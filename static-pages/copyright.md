As noted in the footer, this website and its <a href="https://gitlab.com/punkish/mielho-app" target="blank">code</a> are in the public domain. Any one can view the metadata for our entire catalog, and the metadata (all the stuff you can view *without* logging in) is also in the public domain. Many of the listings are fully viewable and downloadable, but the copyright in them is held by the individual contributor and made available under different, approved open licenses. The details of a few listings may only be visible *after* <a href="/login" class="view">logging in</a>. Details of a few listings may not be visible at all for a temporary embargo period set by their contributors. The license and the embargo period, if any, are determined by the contributors, however, *all* our contributors pledge to make *all* their works available under an open license.

Make sure to read other stuff our lawyers make us say:

- <a href="/terms-of-service" class="view">Terms of Service</a>
- <a href="/terms-of-use" class="view">Terms of Use</a>
- <a href="/privacy-statement" class="view">Privacy Statement</a>
