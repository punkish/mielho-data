create or replace view results_cypress_ep.cms148v3_all as SELECT patient_base.base_patient_id, patient_base."so_7_OccurrenceADiabetesVisit2_patient_id", patient_base."so_7_OccurrenceADiabetesVisit2_audit_key_value", patient_base."so_7_OccurrenceADiabetesVisit2_start_dt", patient_base."so_7_OccurrenceADiabetesVisit2_end_dt", patient_base."so_4_OccurrenceADiabetesVisit2__patient_id", patient_base."so_4_OccurrenceADiabetesVisit2__audit_key_value", patient_base."so_4_OccurrenceADiabetesVisit2__start_dt", patient_base."so_4_OccurrenceADiabetesVisit2__end_dt", patient_base.row_number_4, patient_base."so_3_OccurrenceADiabetes1_preco_patient_id", patient_base."so_3_OccurrenceADiabetes1_preco_audit_key_value", patient_base."so_3_OccurrenceADiabetes1_preco_start_dt", patient_base."so_3_OccurrenceADiabetes1_preco_end_dt", patient_base."so_1_OccurrenceADiabetes1_preco_patient_id", patient_base."so_1_OccurrenceADiabetes1_preco_audit_key_value", patient_base."so_1_OccurrenceADiabetes1_preco_start_dt", patient_base."so_1_OccurrenceADiabetes1_preco_end_dt", (EXISTS
(SELECT "pc_28_PatientCharacteristicBirt".patient_id
 FROM
   (SELECT patient_characteristic_birthdate_1.patient_id AS patient_id
    FROM hqmf_cypress_ep.patient_characteristic_birthdate AS patient_characteristic_birthdate_1
    JOIN
    valuesets.code_lists AS code_lists_1 ON code_lists_1.code = patient_characteristic_birthdate_1.code
    WHERE code_lists_1.code_list_id = '2.16.840.1.113883.3.560.100.4'
      AND patient_characteristic_birthdate_1.start_dt + CAST('1825 days, 0:00:00' AS INTERVAL) < CAST('2013-01-01T00:00:00' AS TIMESTAMP WITHOUT TIME ZONE)
      AND patient_base.base_patient_id = patient_characteristic_birthdate_1.patient_id) AS "pc_28_PatientCharacteristicBirt"))
AND (EXISTS
     (SELECT "pc_30_PatientCharacteristicBirt".patient_id
      FROM
        (SELECT patient_characteristic_birthdate_2.patient_id AS patient_id
         FROM hqmf_cypress_ep.patient_characteristic_birthdate AS patient_characteristic_birthdate_2
         JOIN
         valuesets.code_lists AS code_lists_2 ON code_lists_2.code = patient_characteristic_birthdate_2.code
         WHERE code_lists_2.code_list_id = '2.16.840.1.113883.3.560.100.4'
           AND patient_characteristic_birthdate_2.start_dt < CAST('2013-01-01T00:00:00' AS TIMESTAMP WITHOUT TIME ZONE)
           AND CAST('2013-01-01T00:00:00' AS TIMESTAMP WITHOUT TIME ZONE) < patient_characteristic_birthdate_2.start_dt + CAST('6205 days, 0:00:00' AS INTERVAL)
           AND patient_base.base_patient_id = patient_characteristic_birthdate_2.patient_id) AS "pc_30_PatientCharacteristicBirt"))
AND (EXISTS
     (SELECT "pc_32_EncounterPerformedDiabete".patient_id
      FROM
        (SELECT encounter_performed_1.patient_id AS patient_id
         FROM hqmf_cypress_ep.encounter_performed AS encounter_performed_1
         JOIN
         valuesets.code_lists AS code_lists_3 ON code_lists_3.code = encounter_performed_1.code
         WHERE code_lists_3.code_list_id = '2.16.840.1.113883.3.464.1003.103.12.1012'
           AND patient_base.base_patient_id = encounter_performed_1.patient_id
           AND encounter_performed_1.start_dt + CAST('360 days, 0:00:00' AS INTERVAL) < patient_base."so_7_OccurrenceADiabetesVisit2_start_dt") AS "pc_32_EncounterPerformedDiabete"))
AND (EXISTS
     (SELECT "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_patient_id",
             "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_audit_key_value",
             "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_start_dt",
             "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_end_dt"
      FROM
        (SELECT diagnosis_active_1.patient_id AS "so_3_OccurrenceADiabetes1_preco_patient_id",
                diagnosis_active_1.audit_key_value AS "so_3_OccurrenceADiabetes1_preco_audit_key_value",
                diagnosis_active_1.start_dt AS "so_3_OccurrenceADiabetes1_preco_start_dt",
                diagnosis_active_1.end_dt AS "so_3_OccurrenceADiabetes1_preco_end_dt"
         FROM hqmf_cypress_ep.diagnosis_active AS diagnosis_active_1
         JOIN
         valuesets.code_lists AS code_lists_4 ON code_lists_4.code = diagnosis_active_1.code
         WHERE code_lists_4.code_list_id = '2.16.840.1.113883.3.464.1003.103.12.1001'
           AND patient_base.base_patient_id = diagnosis_active_1.patient_id
           AND diagnosis_active_1.audit_key_value = patient_base."so_3_OccurrenceADiabetes1_preco_audit_key_value"
           AND patient_base."so_3_OccurrenceADiabetes1_preco_start_dt" < patient_base."so_4_OccurrenceADiabetesVisit2__end_dt") AS "so_3_OccurrenceADiabetes1_preco"))
AND NOT (EXISTS
         (SELECT "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_patient_id",
                 "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_audit_key_value",
                 "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_start_dt",
                 "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_end_dt"
          FROM
            (SELECT diagnosis_active_2.patient_id AS "so_1_OccurrenceADiabetes1_preco_patient_id",
                    diagnosis_active_2.audit_key_value AS "so_1_OccurrenceADiabetes1_preco_audit_key_value",
                    diagnosis_active_2.start_dt AS "so_1_OccurrenceADiabetes1_preco_start_dt",
                    diagnosis_active_2.end_dt AS "so_1_OccurrenceADiabetes1_preco_end_dt"
             FROM hqmf_cypress_ep.diagnosis_active AS diagnosis_active_2
             JOIN
             valuesets.code_lists AS code_lists_5 ON code_lists_5.code = diagnosis_active_2.code
             WHERE code_lists_5.code_list_id = '2.16.840.1.113883.3.464.1003.103.12.1001'
               AND patient_base.base_patient_id = diagnosis_active_2.patient_id
               AND diagnosis_active_2.audit_key_value = patient_base."so_1_OccurrenceADiabetes1_preco_audit_key_value"
               AND patient_base."so_1_OccurrenceADiabetes1_preco_end_dt" < patient_base."so_7_OccurrenceADiabetesVisit2_start_dt") AS "so_1_OccurrenceADiabetes1_preco")) AS "IPP",

(SELECT true AS anon_1) AS "DENOM",
      EXISTS
(SELECT "pc_40_LaboratoryTestResultHba1c".patient_id
 FROM
   (SELECT laboratory_test_1.patient_id AS patient_id
    FROM hqmf_cypress_ep.laboratory_test AS laboratory_test_1
    JOIN
    valuesets.code_lists AS code_lists_6 ON code_lists_6.code = laboratory_test_1.code
    WHERE code_lists_6.code_list_id = '2.16.840.1.113883.3.464.1003.198.12.1013'
      AND laboratory_test_1.start_dt >= CAST('2013-01-01T00:00:00' AS TIMESTAMP WITHOUT TIME ZONE)
      AND laboratory_test_1.end_dt <= CAST('2013-12-31T23:59:00' AS TIMESTAMP WITHOUT TIME ZONE)
      AND laboratory_test_1.value IS NOT NULL
      AND patient_base.base_patient_id = laboratory_test_1.patient_id) AS "pc_40_LaboratoryTestResultHba1c") AS "NUMER"
FROM
(SELECT base_patients.patient_id AS base_patient_id,
        "so_7_OccurrenceADiabetesVisit2"."so_7_OccurrenceADiabetesVisit2_patient_id" AS "so_7_OccurrenceADiabetesVisit2_patient_id",
        "so_7_OccurrenceADiabetesVisit2"."so_7_OccurrenceADiabetesVisit2_audit_key_value" AS "so_7_OccurrenceADiabetesVisit2_audit_key_value",
        "so_7_OccurrenceADiabetesVisit2"."so_7_OccurrenceADiabetesVisit2_start_dt" AS "so_7_OccurrenceADiabetesVisit2_start_dt",
        "so_7_OccurrenceADiabetesVisit2"."so_7_OccurrenceADiabetesVisit2_end_dt" AS "so_7_OccurrenceADiabetesVisit2_end_dt",
        "so_4_OccurrenceADiabetesVisit2_"."so_4_OccurrenceADiabetesVisit2__patient_id" AS "so_4_OccurrenceADiabetesVisit2__patient_id",
        "so_4_OccurrenceADiabetesVisit2_"."so_4_OccurrenceADiabetesVisit2__audit_key_value" AS "so_4_OccurrenceADiabetesVisit2__audit_key_value",
        "so_4_OccurrenceADiabetesVisit2_"."so_4_OccurrenceADiabetesVisit2__start_dt" AS "so_4_OccurrenceADiabetesVisit2__start_dt",
        "so_4_OccurrenceADiabetesVisit2_"."so_4_OccurrenceADiabetesVisit2__end_dt" AS "so_4_OccurrenceADiabetesVisit2__end_dt",
        "so_4_OccurrenceADiabetesVisit2_".row_number_4 AS row_number_4,
        "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_patient_id" AS "so_3_OccurrenceADiabetes1_preco_patient_id",
        "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_audit_key_value" AS "so_3_OccurrenceADiabetes1_preco_audit_key_value",
        "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_start_dt" AS "so_3_OccurrenceADiabetes1_preco_start_dt",
        "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_end_dt" AS "so_3_OccurrenceADiabetes1_preco_end_dt",
        "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_patient_id" AS "so_1_OccurrenceADiabetes1_preco_patient_id",
        "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_audit_key_value" AS "so_1_OccurrenceADiabetes1_preco_audit_key_value",
        "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_start_dt" AS "so_1_OccurrenceADiabetes1_preco_start_dt",
        "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_end_dt" AS "so_1_OccurrenceADiabetes1_preco_end_dt"
 FROM hqmf_cypress_ep.patients AS base_patients
 LEFT OUTER JOIN
   (SELECT encounter_performed_2.patient_id AS "so_7_OccurrenceADiabetesVisit2_patient_id",
           encounter_performed_2.audit_key_value AS "so_7_OccurrenceADiabetesVisit2_audit_key_value",
           encounter_performed_2.start_dt AS "so_7_OccurrenceADiabetesVisit2_start_dt",
           encounter_performed_2.end_dt AS "so_7_OccurrenceADiabetesVisit2_end_dt"
    FROM hqmf_cypress_ep.encounter_performed AS encounter_performed_2
    JOIN
    valuesets.code_lists AS code_lists_7 ON code_lists_7.code = encounter_performed_2.code
    WHERE code_lists_7.code_list_id = '2.16.840.1.113883.3.464.1003.103.12.1012') AS "so_7_OccurrenceADiabetesVisit2" ON "so_7_OccurrenceADiabetesVisit2"."so_7_OccurrenceADiabetesVisit2_patient_id" = base_patients.patient_id
 LEFT OUTER JOIN
   (SELECT anon_2."so_4_OccurrenceADiabetesVisit2__patient_id" AS "so_4_OccurrenceADiabetesVisit2__patient_id",
           anon_2."so_4_OccurrenceADiabetesVisit2__audit_key_value" AS "so_4_OccurrenceADiabetesVisit2__audit_key_value",
           anon_2."so_4_OccurrenceADiabetesVisit2__start_dt" AS "so_4_OccurrenceADiabetesVisit2__start_dt",
           anon_2."so_4_OccurrenceADiabetesVisit2__end_dt" AS "so_4_OccurrenceADiabetesVisit2__end_dt",
           anon_2.row_number_4 AS row_number_4
    FROM
      (SELECT encounter_performed_3.patient_id AS "so_4_OccurrenceADiabetesVisit2__patient_id",
              encounter_performed_3.audit_key_value AS "so_4_OccurrenceADiabetesVisit2__audit_key_value",
              encounter_performed_3.start_dt AS "so_4_OccurrenceADiabetesVisit2__start_dt",
              encounter_performed_3.end_dt AS "so_4_OccurrenceADiabetesVisit2__end_dt",
              row_number() OVER (PARTITION BY encounter_performed_3.patient_id
                                 ORDER BY coalesce(encounter_performed_3.start_dt, encounter_performed_3.end_dt) DESC) AS row_number_4
       FROM hqmf_cypress_ep.encounter_performed AS encounter_performed_3
       JOIN
       valuesets.code_lists AS code_lists_8 ON code_lists_8.code = encounter_performed_3.code
       WHERE code_lists_8.code_list_id = '2.16.840.1.113883.3.464.1003.103.12.1012'
         AND encounter_performed_3.start_dt >= CAST('2013-01-01T00:00:00' AS TIMESTAMP WITHOUT TIME ZONE)
         AND encounter_performed_3.end_dt <= CAST('2013-12-31T23:59:00' AS TIMESTAMP WITHOUT TIME ZONE)) AS anon_2
    WHERE anon_2.row_number_4 = 1) AS "so_4_OccurrenceADiabetesVisit2_" ON "so_4_OccurrenceADiabetesVisit2_"."so_4_OccurrenceADiabetesVisit2__patient_id" = base_patients.patient_id
 AND "so_4_OccurrenceADiabetesVisit2_"."so_4_OccurrenceADiabetesVisit2__audit_key_value" = "so_7_OccurrenceADiabetesVisit2"."so_7_OccurrenceADiabetesVisit2_audit_key_value"
 LEFT OUTER JOIN
   (SELECT diagnosis_active_1.patient_id AS "so_3_OccurrenceADiabetes1_preco_patient_id",
           diagnosis_active_1.audit_key_value AS "so_3_OccurrenceADiabetes1_preco_audit_key_value",
           diagnosis_active_1.start_dt AS "so_3_OccurrenceADiabetes1_preco_start_dt",
           diagnosis_active_1.end_dt AS "so_3_OccurrenceADiabetes1_preco_end_dt"
    FROM hqmf_cypress_ep.diagnosis_active AS diagnosis_active_1
    JOIN
    valuesets.code_lists AS code_lists_4 ON code_lists_4.code = diagnosis_active_1.code
    WHERE code_lists_4.code_list_id = '2.16.840.1.113883.3.464.1003.103.12.1001') AS "so_3_OccurrenceADiabetes1_preco" ON "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_patient_id" = base_patients.patient_id
 LEFT OUTER JOIN
   (SELECT diagnosis_active_2.patient_id AS "so_1_OccurrenceADiabetes1_preco_patient_id",
           diagnosis_active_2.audit_key_value AS "so_1_OccurrenceADiabetes1_preco_audit_key_value",
           diagnosis_active_2.start_dt AS "so_1_OccurrenceADiabetes1_preco_start_dt",
           diagnosis_active_2.end_dt AS "so_1_OccurrenceADiabetes1_preco_end_dt"
    FROM hqmf_cypress_ep.diagnosis_active AS diagnosis_active_2
    JOIN
    valuesets.code_lists AS code_lists_5 ON code_lists_5.code = diagnosis_active_2.code
    WHERE code_lists_5.code_list_id = '2.16.840.1.113883.3.464.1003.103.12.1001') AS "so_1_OccurrenceADiabetes1_preco" ON "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_patient_id" = base_patients.patient_id
 AND "so_1_OccurrenceADiabetes1_preco"."so_1_OccurrenceADiabetes1_preco_audit_key_value" = "so_3_OccurrenceADiabetes1_preco"."so_3_OccurrenceADiabetes1_preco_audit_key_value") AS patient_base;

CREATE TABLE results_cypress_ep.cms148v3_measure_metadata (
	measure_id VARCHAR(256), 
	hqmf_id VARCHAR(256), 
	hqmf_set_id VARCHAR(256), 
	hqmf_version_number INTEGER, 
	population_name VARCHAR(256), 
	title VARCHAR(256), 
	description VARCHAR(256), 
	cms_id VARCHAR(256), 
	stratification VARCHAR(256), 
	measure_period_start VARCHAR(256), 
	measure_period_end VARCHAR(256)
)


;


CREATE TABLE results_cypress_ep.cms148v3_population_metadata (
	measure_id VARCHAR(256), 
	measure_hqmf_id VARCHAR(256), 
	population_name VARCHAR(256), 
	population_criterion_type VARCHAR(256), 
	population_criterion_name VARCHAR(256), 
	hqmf_id VARCHAR(256)
)


;

INSERT INTO results_cypress_ep.cms148v3_measure_metadata (measure_id, hqmf_id, hqmf_set_id, hqmf_version_number, population_name, title, description, cms_id, stratification, measure_period_start, measure_period_end) SELECT CAST('0060' AS VARCHAR(256)) AS measure_id, CAST('40280381-4600-425F-0146-1F7129420F2A' AS VARCHAR(256)) AS hqmf_id, CAST('95FB767E-0CB2-4778-B5FF-6BA9A53FA28E' AS VARCHAR(256)) AS hqmf_set_id, CAST(3 AS INTEGER) AS hqmf_version_number, CAST(NULL AS VARCHAR(256)) AS population_name, CAST('Hemoglobin A1c Test for Pediatric Patients' AS VARCHAR(256)) AS title, CAST('Percentage of patients 5-17 years of age with diabetes with an HbA1c test during the measurement period' AS VARCHAR(256)) AS description, CAST('CMS148v3' AS VARCHAR(256)) AS cms_id, CAST(NULL AS VARCHAR(256)) AS stratification, CAST('20130101' AS VARCHAR(256)) AS measure_period_start, CAST('20131231' AS VARCHAR(256)) AS measure_period_end
;

INSERT INTO results_cypress_ep.cms148v3_population_metadata (measure_id, measure_hqmf_id, population_name, population_criterion_type, population_criterion_name, hqmf_id) SELECT CAST('0060' AS VARCHAR(256)) AS measure_id, CAST('40280381-4600-425F-0146-1F7129420F2A' AS VARCHAR(256)) AS measure_hqmf_id, CAST(NULL AS VARCHAR(256)) AS population_name, CAST('IPP' AS VARCHAR(256)) AS population_crierion_type, CAST('IPP' AS VARCHAR(256)) AS population_criterion_name, CAST('9B0D7655-203D-48F0-AB69-28C0BEC39FC5' AS VARCHAR(256)) AS hqmf_id
;

INSERT INTO results_cypress_ep.cms148v3_population_metadata (measure_id, measure_hqmf_id, population_name, population_criterion_type, population_criterion_name, hqmf_id) SELECT CAST('0060' AS VARCHAR(256)) AS measure_id, CAST('40280381-4600-425F-0146-1F7129420F2A' AS VARCHAR(256)) AS measure_hqmf_id, CAST(NULL AS VARCHAR(256)) AS population_name, CAST('DENOM' AS VARCHAR(256)) AS population_crierion_type, CAST('DENOM' AS VARCHAR(256)) AS population_criterion_name, CAST('2FB8B08A-9863-430A-8C01-8053D362EA27' AS VARCHAR(256)) AS hqmf_id
;

INSERT INTO results_cypress_ep.cms148v3_population_metadata (measure_id, measure_hqmf_id, population_name, population_criterion_type, population_criterion_name, hqmf_id) SELECT CAST('0060' AS VARCHAR(256)) AS measure_id, CAST('40280381-4600-425F-0146-1F7129420F2A' AS VARCHAR(256)) AS measure_hqmf_id, CAST(NULL AS VARCHAR(256)) AS population_name, CAST('NUMER' AS VARCHAR(256)) AS population_crierion_type, CAST('NUMER' AS VARCHAR(256)) AS population_criterion_name, CAST('AF39DC8C-4EC5-4AE0-A477-76B3FFFD2C6A' AS VARCHAR(256)) AS hqmf_id
;


CREATE TABLE results_cypress_ep.cms148v3_patient_summary (
	population_id VARCHAR(256), 
	title VARCHAR(256), 
	strat_id VARCHAR(256), 
	patient_id INTEGER, 
	effective_ipp BOOLEAN, 
	effective_denom BOOLEAN, 
	effective_denex BOOLEAN, 
	effective_numer BOOLEAN, 
	effective_denexcep BOOLEAN, 
	strat VARCHAR(256)
)


;

INSERT INTO results_cypress_ep.cms148v3_patient_summary (population_id, title, strat_id, patient_id, effective_ipp, effective_denom, effective_denex, effective_numer, effective_denexcep, strat) SELECT DISTINCT anon_1.population_id, anon_1.title, anon_1.strat_id, anon_1.patient_id, anon_1.effective_ipp, anon_1.effective_denom, anon_1.effective_denex, anon_1.effective_numer, anon_1.effective_denexcep, anon_1.strat 
FROM (SELECT CAST(NULL AS VARCHAR(256)) AS population_id, CAST(NULL AS VARCHAR(256)) AS title, CAST(NULL AS VARCHAR(256)) AS strat_id, base_patient_id AS patient_id, "IPP" AS effective_ipp, "IPP" AND "DENOM" AS effective_denom, CAST(NULL AS BOOLEAN) AS effective_denex, "IPP" AND "DENOM" AND "NUMER" AS effective_numer, CAST(NULL AS BOOLEAN) AS effective_denexcep, CAST(NULL AS BOOLEAN) AS strat, rank() OVER (PARTITION BY base_patient_id ORDER BY ("IPP" AND "DENOM" AND "NUMER") DESC, ("IPP" AND "DENOM") DESC, "IPP" DESC) AS rank 
FROM results_cypress_ep.cms148v3_all 
WHERE "IPP") AS anon_1 
WHERE anon_1.rank = 1
;

